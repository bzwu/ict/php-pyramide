<?php

if ( php_sapi_name() !== 'cli' ) {
	die( 'This is a commandline script only' . PHP_EOL );
}

$columns = 10;

for ( $colCount = 1; $colCount <= $columns; $colCount++ ) {
	$rows = $colCount;
	for ( $rowCount = 1; $rowCount <= $rows; $rowCount++ ) {
		if ( $rowCount % 3 === 0 ) {
			echo '|';
		} else {
			echo '*';
		}
	}
	echo PHP_EOL;
}

die( PHP_EOL );